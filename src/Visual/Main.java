package Visual;

import java.awt.EventQueue;

import logica.Contador;

public class Main {

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Contador cont = new Contador();
					Ventana window = new Ventana(cont);
					Reproductor musica = new Reproductor();
					musica.reproducir();
				
					window.frame.setVisible(true);
				
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}


}
